import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:top_flutter_repo/constants/colors.dart';
import 'package:top_flutter_repo/helpers/network_services.dart';
import '../screens/home.dart';
import 'helpers/scheduler.dart';

void main() async {
  await GetStorage.init();
  WidgetsFlutterBinding.ensureInitialized();
  NetworkServices.initConnectivity();
  await ScreenUtil.ensureScreenSize();
  await initializeService();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(360, 690),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (context , child) {
        return GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            WidgetsBinding.instance.focusManager.primaryFocus?.unfocus();
          },
          child: GetMaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Top Flutter Repos',
            theme: ThemeData(
              primarySwatch: CustomColors.primaryBlack,
              textTheme: TextTheme(bodyText2: TextStyle(fontSize: 16.sp)),
            ),
            home: child,
          ),
        );
      },
      child: const HomesScreen(),
    );
  }
}