import '../constants/database_constants.dart';

class GithubRepoModel {
  int? id;
  int? repoId;
  String? repoName;
  String? description;
  int? starsCount;
  String? updatedAt;
  String? language;
  String? avatarUrl;
  String? ownerName;
  List? topics;

  GithubRepoModel(
      {this.id,
      this.repoId,
      this.repoName,
      this.description,
      this.starsCount,
      this.updatedAt,
      this.language,
      this.avatarUrl,
      this.ownerName,
      this.topics});

  GithubRepoModel.fromJson(Map<String, dynamic> json) {
    repoId = json['id'];
    repoName = json['name'];
    description = json['description'];
    starsCount = json['stargazers_count'];
    updatedAt = json['updated_at'];
    language = json['language'];
    avatarUrl = json['owner']['avatar_url'];
    ownerName = json['owner']['login'];
    topics = json['topics'];
  }
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};
    if (id != null) {
      map[DbConstants.colId] = id;
    }
    map[DbConstants.colGitRepoId] = repoId;
    map[DbConstants.colName] = repoName;
    map[DbConstants.colDescription] = description;
    map[DbConstants.colStarsCount] = starsCount;
    map[DbConstants.colUpdatedAt] = updatedAt;
    map[DbConstants.colLanguage] = language;
    map[DbConstants.colAvatarUrl] = avatarUrl;
    map[DbConstants.colOwnerName] = ownerName;
    map[DbConstants.colTopics] = topics?.join(', ');
    return map;
  }
}
