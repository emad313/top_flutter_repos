import '../constants/database_constants.dart';

class RepoDataModel {
  int? id;
  String? repoName;
  String? description;
  int? starsCount;
  String? updatedAt;
  String? language;
  String? avatarUrl;
  String? ownerName;
  List? topics;

  RepoDataModel(
      {this.id,
      this.repoName,
      this.description,
      this.starsCount,
      this.updatedAt,
      this.language,
      this.avatarUrl,
      this.ownerName,
      this.topics});

  RepoDataModel.fromJson(Map<String, dynamic> json) {
    id = json[DbConstants.colId];
    repoName = json[DbConstants.colName];
    description = json[DbConstants.colDescription];
    starsCount = json[DbConstants.colStarsCount];
    updatedAt = json[DbConstants.colUpdatedAt];
    language = json[DbConstants.colLanguage];
    avatarUrl = json[DbConstants.colAvatarUrl];
    ownerName = json[DbConstants.colOwnerName];
    topics = json[DbConstants.colTopics].split(',');
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};
    if (id != null) {
      map[DbConstants.colId] = id;
    }
    map[DbConstants.colName] = repoName;
    map[DbConstants.colDescription] = description;
    map[DbConstants.colStarsCount] = starsCount;
    map[DbConstants.colUpdatedAt] = updatedAt;
    map[DbConstants.colLanguage] = language;
    map[DbConstants.colAvatarUrl] = avatarUrl;
    map[DbConstants.colOwnerName] = ownerName;
    map[DbConstants.colTopics] = topics?.join(', ');
    return map;
  }
}
