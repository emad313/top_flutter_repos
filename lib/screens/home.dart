import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:top_flutter_repo/constants/colors.dart';
import 'package:top_flutter_repo/constants/database_constants.dart';
import 'package:top_flutter_repo/controllers/details_controller.dart';
import 'package:top_flutter_repo/repositories/api_request.dart';
import 'package:top_flutter_repo/widgets/default_layout.dart';

import '../controllers/datacontroller.dart';
import '../widgets/repo_card.dart';
import 'github_repo_details.dart';

class HomesScreen extends StatefulWidget {
  const HomesScreen({super.key});

  @override
  State<HomesScreen> createState() => _HomesScreenState();
}

class _HomesScreenState extends State<HomesScreen> {
  var dataController = Get.put(DataController());
  // sorting options
  String sortBy = DbConstants.colStarsCount;
  final List<PopupMenuItem> _sortOptions = [
    const PopupMenuItem(
      value: DbConstants.colUpdatedAt,
      child: Text("Last update"),
    ),
    const PopupMenuItem(
      value: DbConstants.colStarsCount,
      child: Text("Most stars"),
    ),
  ];

  @override
  void initState() {
    super.initState();
    sortBy = dataController.readFromStorage();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      widgets: [
      Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            'Sort by',
            style: TextStyle(
                color: CustomColors.black,
                fontSize: 14.0.sp,
                fontWeight: FontWeight.w400),
          ),
          SizedBox(width: 8.0.w),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 8.0.w),
            decoration: BoxDecoration(
                color: CustomColors.blue,
                borderRadius: BorderRadius.circular(8.0.r)),
            child: Text(
              sortBy.replaceAll('_', ' '),
              style: TextStyle(
                  color: CustomColors.white,
                  fontSize: 14.0.sp,
                  fontWeight: FontWeight.w400),
            ),
          ),
          SizedBox(width: 8.0.w),
          Container(
            height: 40.0.h,
            width: 40.0.w,
            decoration: BoxDecoration(
                color: CustomColors.white,
                borderRadius: BorderRadius.circular(8.0.r),
                boxShadow: [
                  BoxShadow(
                      color: CustomColors.black.withOpacity(0.1),
                      spreadRadius: -10,
                      blurRadius: 12,
                      offset: const Offset(0, 5))
                ]),
            child: PopupMenuButton(
                offset: const Offset(0, 10),
                initialValue: sortBy,
                icon: Icon(Icons.filter_list, size: 24.0.sp),
                enabled: true,
                onSelected: (value) {
                  setState(() {
                    dataController.writeToStorage(value);
                    sortBy = value;
                    dataController.fetchReposFromLocalDB(
                      sortBy: sortBy
                    );
                  });
                },
                itemBuilder: (context) => _sortOptions),
          ),
        ],
      ),
      Divider(
        color: CustomColors.black.withOpacity(0.1),
        thickness: 1.0.h,
      ),
      Expanded(
        child: GetX<DataController>(
          init: DataController(),
          initState: (_) {
            dataController.fetchReposFromLocalDB(sortBy: sortBy);
          },
          builder: (controller) {
            return controller.reposList.isNotEmpty ? ListView.builder(
              controller: controller.scrollController,
              physics: const BouncingScrollPhysics(),
              itemCount: controller.reposList.length,
              itemBuilder: (context, index) {
                return  RepoCard(
                  onTap: (){
                    Get.lazyPut<DetailsController>(
                    tag: controller.reposList[index].id.toString(),
                    () => DetailsController(id: controller.reposList[index].id));                   
                    Get.to(() => GitHubRepoDetails(
                      id: controller.reposList[index].id,
                      name: controller.reposList[index].repoName,
                    ));
                  },
                  title: controller.reposList[index].repoName,
                  description: controller.reposList[index].description,
                  language: controller.reposList[index].language,
                  stars: controller.reposList[index].starsCount,
                  date: controller.reposList[index].updatedAt,
                  tagList: controller.reposList[index].topics
                );
              },
            ) : Center(
              child: Text(
                'No data found',
                style: TextStyle(
                  color: CustomColors.black,
                  fontSize: 14.0.sp,
                  fontWeight: FontWeight.w400
                ),
              )
            );
          }
        ),
      ),
    ]);
  }
}
