import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:top_flutter_repo/constants/colors.dart';
import 'package:top_flutter_repo/controllers/details_controller.dart';
import 'package:top_flutter_repo/widgets/nav_bar.dart';

class GitHubRepoDetails extends StatefulWidget {
  final int? id;
  final String? name;
  const GitHubRepoDetails({
    super.key,
    required this.id,
    required this.name,
  });

  @override
  State<GitHubRepoDetails> createState() => _GitHubRepoDetailsState();
}

class _GitHubRepoDetailsState extends State<GitHubRepoDetails> {

  @override
  Widget build(BuildContext context) {
   DetailsController  _detailController = Get.put<DetailsController>(DetailsController(id: widget.id));
    return Scaffold(
      appBar: NavBar(
        backButton: true,
        isTitle: true,
        title: widget.name.toString(),
      ),
      body: Padding(
          padding: EdgeInsets.only(left: 8.0.w, right: 8.0.w, top: 8.0.h),
          child: _detailController.isDetailLoading.value
              ? GetX<DetailsController>(
                tag: widget.id.toString(),
                  init: DetailsController(id: widget.id),
                  initState: (_) {},
                  builder: (detailsController) {
                    return ListView(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              padding: EdgeInsets.all(5.0.h),
                              height: 150.0.h,
                              width: 150.0.w,
                              decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                color: CustomColors.black,
                              ),
                              child: Container(
                                height: 90.0.h,
                                width: 90.0.w,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                    image: CachedNetworkImageProvider(
                                      detailsController
                                          .repoDetails.value.avatarUrl
                                          .toString()),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 10.0.h),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.code,
                              color: CustomColors.black,
                              size: 24.0.sp,
                            ),
                            SizedBox(width: 5.0.w),
                            Text(
                              detailsController.repoDetails.value.repoName
                                  .toString(),
                              style: TextStyle(
                                fontSize: 22.0.sp,
                                color: CustomColors.blue,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 10.0.h),
                        // Last Updated
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.update,
                              color: CustomColors.black,
                              size: 16.0.sp,
                            ),
                            SizedBox(width: 4.0.w),
                            Text(
                                DateFormat('MM-dd-yy hh:s').format(
                                    DateTime.parse(detailsController
                                        .repoDetails.value.updatedAt
                                        .toString())),
                                style: TextStyle(
                                  fontSize: 12.0.sp,
                                  color: CustomColors.black,
                                )),
                          ],
                        ),
                        SizedBox(height: 10.0.h),
                        // Description
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                              child: Text(
                                detailsController.repoDetails.value.description
                                    .toString(),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 12.0.sp,
                                  color: CustomColors.black,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    );
                  })
              : const Center(
                  child: CircularProgressIndicator(
                    color: CustomColors.blue,
                  ),
                )),
    );
  }
}
