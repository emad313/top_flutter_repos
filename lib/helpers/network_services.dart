import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:top_flutter_repo/constants/colors.dart';
import 'dart:developer' as developer;

final Connectivity _connectivity = Connectivity();
late StreamSubscription<ConnectivityResult> _connectivitySubscription;
late ConnectivityResult result = ConnectivityResult.none;
final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();

class NetworkServices {
  static initConnectivity() async {
    try {
      result = await _connectivity.checkConnectivity();
      onConnectivityChanged();
    } on PlatformException catch (e) {
      developer.log('Couldn\'t check connectivity status', error: e);
      return;
    }
    return checkConnectionStatus();
  }

  static onConnectivityChanged() {
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen((ConnectivityResult onChanegeResult) {
      if (onChanegeResult != ConnectivityResult.wifi && onChanegeResult != ConnectivityResult.mobile) {
        showSnackBar();
      } else {
        result = onChanegeResult;
        checkConnectionStatus();
        scaffoldMessengerKey.currentState!.hideCurrentSnackBar();
    }
    checkConnectionStatus();
    });
  }

  static checkConnectionStatus() {
    if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {
      return true;
    } else {
      return false;
    }
  }

  static showSnackBar(){
    Get.snackbar('Error', 'Your App is Offline', backgroundColor: CustomColors.red, colorText: CustomColors.white);
  }

  connectivitySubscriptionCancel() {
    _connectivitySubscription.cancel();
  }

}