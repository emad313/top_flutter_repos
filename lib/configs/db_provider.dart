import 'dart:io';
import 'package:path/path.dart' as p; 
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import '../constants/database_constants.dart';
import '../models/github_repo_model.dart';
import '../models/repo_data_model.dart';

class DatabaseProvider{
  DatabaseProvider._();
  static final DatabaseProvider db = DatabaseProvider._();
  static Database? _database;

  Future<Database?> get database async {
    if (_database != null) return _database;
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory(); 
      String path = p.join(documentsDirectory.path, "database.db");
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute('CREATE TABLE ${DbConstants.tableName} ('
          '${DbConstants.colId} INTEGER PRIMARY KEY AUTOINCREMENT,'
          '${DbConstants.colGitRepoId} INTEGER,'
          '${DbConstants.colName} TEXT,'
          '${DbConstants.colDescription} TEXT,'
          '${DbConstants.colStarsCount} INTEGER,'
          '${DbConstants.colUpdatedAt} TEXT,'
          '${DbConstants.colLanguage} TEXT,'
          '${DbConstants.colAvatarUrl} TEXT,'
          '${DbConstants.colOwnerName} TEXT,'
          '${DbConstants.colTopics} TEXT'
          ')');
    });
  }

  // Check if the database is exist or not
  Future<bool> isDatabaseExist() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory(); 
    String path = p.join(documentsDirectory.path, "database.db");
    return await databaseExists(path);
  }

  insert(GithubRepoModel githubRepoModel) async {
    final db = await database;
    var raw = await db!.insert(
      DbConstants.tableName,
      githubRepoModel.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return raw;
  }

  Future insertAll(List<GithubRepoModel> githubRepoModelList) async {
    try {
      final db = await database;
    var batch = db!.batch();
    for (var githubRepoModel in githubRepoModelList) {
      batch.insert(
        DbConstants.tableName,
        githubRepoModel.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
    }
    await batch.commit(noResult: true);
    } catch (e) {
      rethrow;
    }
  }

  
  getGitRepoList({String sortBy = DbConstants.colStarsCount, int limit = 10, String order = 'DESC'}) async {
    final db = await database;
    var res = await db!.query(
      DbConstants.tableName,
      orderBy: '$sortBy $order',
      limit: limit
    );
    int? count = Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT(*) FROM ${DbConstants.tableName}'));
    List<RepoDataModel> list =
        res.isNotEmpty ? res.map((c) => RepoDataModel.fromJson(c)).toList() : [];
      Object result = {'list': list, 'count': count};
    return result;
  }

  Future<List<Map<String, dynamic>>> getAll() async {
    final db = await database;
    var res = await db!.query(DbConstants.tableName);
    return res;
  }

  // Delete all
  deleteAll() async {
    final db = await database;
    db!.delete(DbConstants.tableName);
  }

  // Delete Database
  deleteDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory(); 
    String path = p.join(documentsDirectory.path, "database.db");
    await deleteDatabase(path);
  }

  // Delete by id
  deleteById(int id) async {
    final db = await database;
    db!.delete(
      DbConstants.tableName,
      where: '${DbConstants.colId} = ?',
      whereArgs: [id],
    );
  }

  // Update by id
  updateById(int id, RepoDataModel githubRepoModel) async {
    final db = await database;
    var res = await db!.update(
      DbConstants.tableName,
      githubRepoModel.toMap(),
      where: '${DbConstants.colId} = ?',
      whereArgs: [id],
    );
    return res;
  }

  // Get by id
  getById(int id) async {
    final db = await database;
    var res = await db!.query(
      DbConstants.tableName,
      where: '${DbConstants.colId} = ?',
      whereArgs: [id],
    );
    return res.isNotEmpty ? RepoDataModel.fromJson(res.first) : null;
  }

  // Update all
  updateAll(List<GithubRepoModel> githubRepoModelList) async {
    try {
      final db = await database;
    var batch = db!.batch();
    for (var githubRepoModel in githubRepoModelList) {
      batch.update(
        DbConstants.tableName,
        githubRepoModel.toMap(),
        where: '${DbConstants.colGitRepoId} = ?',
        whereArgs: [githubRepoModel.repoId],
      );
    }
    await batch.commit(noResult: true);
    } catch (e) {
      rethrow;
    }
  }

  // insert or replace all

  insertOrReplaceAll(List<GithubRepoModel> githubRepoModelList) async {
    try {
      final db = await database;
    var batch = db!.batch();
    for (var githubRepoModel in githubRepoModelList) {
      // find if the record is already exist or not
      var res = await db.query(
        DbConstants.tableName,
        where: '${DbConstants.colGitRepoId} = ?',
        whereArgs: [githubRepoModel.repoId],
      );
      if (res.isNotEmpty) {
        batch.update(
          DbConstants.tableName,
          githubRepoModel.toMap(),
          where: '${DbConstants.colGitRepoId} = ?',
          whereArgs: [githubRepoModel.repoId],
        );
      } else {
        batch.insert(
          DbConstants.tableName,
          githubRepoModel.toMap(),
          conflictAlgorithm: ConflictAlgorithm.replace,
        );
      }
    }
    await batch.commit(noResult: true);
    } catch (e) {
      rethrow;
    }
  }  

  // Pagination with offset and limit
  getGitRepoListWithPagination(
      {int offset = 0,
      int limit = 10,
      String sortBy = DbConstants.colUpdatedAt,
      String order = 'DESC'}) async {
    final db = await database;
    var res = await db!.rawQuery('SELECT COUNT(*) FROM ${DbConstants.tableName}');
    // List<RepoDataModel> list =
    //     res.isNotEmpty ? res.map((c) => RepoDataModel.fromJson(c)).toList() : [];
    return res[0]['COUNT(*)'];
  }
}