class DbConstants{
  static const String tableName = 'github_repositories';
  static const String colId = 'id';
  static const String colGitRepoId = 'gitrepo_id';
  static const String colName = 'name';
  static const String colDescription = 'description';
  static const String colStarsCount = 'stars_count';
  static const String colUpdatedAt = 'updated_at';
  static const String colLanguage = 'language';
  static const String colAvatarUrl = 'avatar_url';
  static const String colOwnerName = 'owner_name';
  static const String colTopics = 'topics';
}