import 'package:flutter/material.dart';

class CustomColors {
  // Theme Primary Color
  static const int _blackPrimaryValue = 0xFF000000;
  static const MaterialColor primaryBlack = MaterialColor(
  _blackPrimaryValue,
  <int, Color>{
    50: Color(_blackPrimaryValue),
    100: Color(_blackPrimaryValue),
    200: Color(_blackPrimaryValue),
    300: Color(_blackPrimaryValue),
    400: Color(_blackPrimaryValue),
    500: Color(_blackPrimaryValue),
    600: Color(_blackPrimaryValue),
    700: Color(_blackPrimaryValue),
    800: Color(_blackPrimaryValue),
    900: Color(_blackPrimaryValue),
  },
);

// Theme Secondary Colors
static const Color blue = Colors.blue;
static const Color black = Colors.black;
static const Color white = Colors.white;
static const Color red = Colors.red;
static const Color green = Colors.green;
static const Color yellow = Colors.yellow;
static const Color orange = Colors.orange;
static const Color purple = Colors.purple;
static const Color grey = Colors.grey;
}