import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../configs/db_provider.dart';
import '../constants/database_constants.dart';
import '../models/repo_data_model.dart';

class DataController extends GetxController {
  var reposList = <RepoDataModel>[].obs;

  // Get storage
  final storage = GetStorage();

  final _storeKey =  'sortBy';

  int limit = 10;
  int count = 0;
  ScrollController scrollController = ScrollController();
  bool isLoading = false;

  @override
  Future<void> onInit() async {
    super.onInit();
    fetchReposFromLocalDB(
          sortBy: readFromStorage(),
          order: 'DESC',
        );
    
  }

  @override
  Future<void> onReady() async {
    super.onReady();
  }

  // Write to storage
  void writeToStorage(String value) {
    storage.write(_storeKey, value );
  }

  // Read from storage
  String readFromStorage() {
    return storage.read(_storeKey)?? DbConstants.colStarsCount;
  }

    scroll() async {
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent == scrollController.position.pixels) {
        if (count != reposList.length) {
          if (!isLoading) {
          isLoading = true;
          limit += 10;
          fetchReposFromLocalDB(
          sortBy: readFromStorage(),
          order: 'DESC',
          limit: limit,
        );
        }
        }
        
        update();
      }
    });
  }

  void fetchReposFromLocalDB({String sortBy = DbConstants.colStarsCount, String order = 'DESC', int limit = 10}) async {
    DatabaseProvider.db.getGitRepoList(
      order: order,
      sortBy: sortBy,
      limit: limit
    )
        .then((value) {
      reposList.value = value['list'];
      count = value['count'];
      isLoading = false;
      scroll();
    });
  }

  @override
  void onClose() {}
}
