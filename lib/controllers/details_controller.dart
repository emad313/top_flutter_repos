import 'package:get/get.dart';
import 'package:top_flutter_repo/configs/db_provider.dart';

import '../models/repo_data_model.dart';

class DetailsController extends GetxController {
  DetailsController({required this.id});
  late final int? id;
  Rx<RepoDataModel> repoDetails = RepoDataModel().obs;

  var isDetailLoading = false.obs;

  @override
  void onInit() {
    super.onInit();
    fetchDetails();
  }

    @override
  Future<void> onReady() async {
    super.onReady();
  }

  void fetchDetails() async {
    isDetailLoading(true);
    DatabaseProvider.db.getById(id!).then((value) {
      repoDetails.value = value;
    });
  }

  @override
  void onClose() {}
}