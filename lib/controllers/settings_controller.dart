import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class SettingsController extends GetxController {
  final storage = GetStorage();
  final schedulerKey =  'isScheduler';
  final notificationKey =  'isNotification';
  // Switch toggle
  var isScheduler = true.obs;
  var isNotification = true.obs;
  var isHomeLoading = false.obs;

  void schedulerToggle()=>{
    isScheduler.value = isScheduler.value ? false : true,
    storage.write(schedulerKey, isScheduler.value)
  };

  void notificationToggle() => {
    isNotification.value = isNotification.value ? false : true,
    storage.write(notificationKey, isNotification.value)
  };


  @override
  void onInit() {
    super.onInit();
    isScheduler.value = storage.read(schedulerKey)?? true;
    isNotification.value = storage.read(notificationKey)?? true;
  }

    @override
  Future<void> onReady() async {
    super.onReady();
  }


  @override
  void onClose() {}
}