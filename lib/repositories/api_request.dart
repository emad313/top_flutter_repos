import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:top_flutter_repo/constants/api.dart';
import 'package:top_flutter_repo/models/github_repo_model.dart';

import '../configs/db_provider.dart';
import '../controllers/settings_controller.dart';
import '../helpers/network_services.dart';

class APIRequest {
  Map<String, String> headers = {
      'Accept': 'application/vnd.github+json',
    };

  getGitHubRepos() async {
    try{
     if (NetworkServices.checkConnectionStatus()) {
    await http.get(
    Uri.parse(APIConstant.BASE_URL + APIConstant.SEARCH_REPOSITORIES),
    headers: headers).then((response) async {
      if (response.statusCode == 200) {
        SettingsController settings = Get.put(SettingsController());
       var data = jsonDecode(response.body)['items'];
        List<GithubRepoModel> githubRepoModelList = List<GithubRepoModel>.from(data.map((model)=> GithubRepoModel.fromJson(model))).toList();
        if(await DatabaseProvider.db.isDatabaseExist()){
          DatabaseProvider.db.insertOrReplaceAll(githubRepoModelList);
          settings.isHomeLoading(true);
        }
        else{
          DatabaseProvider.db.insertAll(githubRepoModelList);
          settings.isHomeLoading(true);
        }
        
      } else {
        throw Exception('Failed to load data');
      }
      
    });
    }else{
      SettingsController settings = Get.put(SettingsController());
      settings.isHomeLoading(true);
    }
  }catch(e){
    print(e);
  }finally{
    
  }
  }
}
