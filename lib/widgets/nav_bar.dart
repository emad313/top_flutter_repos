import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../constants/colors.dart';

class NavBar extends StatefulWidget implements PreferredSizeWidget {
  final double height;
  final bool transparent;
  final Color bgColor;
  final bool noShadow;
  final bool isTitle;
  final String title;
  final bool backButton;
  final List<Widget> actions;
  const NavBar({super.key,
  this.height = 80.0,
  this.transparent = false,
  this.bgColor = CustomColors.black,
  this.noShadow = false,
  this.isTitle = false,
  this.title = '',
  this.backButton = false,
  this.actions = const <Widget>[],
  });

  @override
  State<NavBar> createState() => _NavBarState();
  
  @override
  Size get preferredSize => Size.fromHeight(height);
}

class _NavBarState extends State<NavBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height.h,
      decoration: BoxDecoration(
            color: !widget.transparent ? widget.bgColor : Colors.transparent,
            boxShadow: [
              BoxShadow(
                  color: !widget.transparent && !widget.noShadow
                      ? CustomColors.black.withOpacity(0.1)
                      : Colors.transparent,
                  spreadRadius: -10,
                  blurRadius: 12,
                  offset: const Offset(0, 5))
            ]),
      child: SafeArea(
        child: Padding(
          padding: EdgeInsets.only(left: 8.0.w, right: 8.0.w),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                      children: [
                        IconButton(
                            icon: Icon(
                                !widget.backButton
                                    ? Icons.notes_rounded
                                    : Icons.arrow_back_ios,
                                color: !widget.transparent
                                    ? (widget.bgColor == CustomColors.white
                                        ? CustomColors.black
                                        : CustomColors.white)
                                    : CustomColors.white,
                                size: 24.0.sp),
                            onPressed: () {
                              if (!widget.backButton) {
                                Scaffold.of(context).openDrawer();
                              } else {
                                Get.back();
                              }
                            }),
                        widget.isTitle
                            ? Text(widget.title,
                                style: TextStyle(
                                    color: !widget.transparent
                                        ? (widget.bgColor == CustomColors.white
                                            ? CustomColors.black
                                            : CustomColors.white)
                                        : CustomColors.white,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 16.0.sp))
                            : Container(),
                      ],
                    ),
                    if(widget.actions.isNotEmpty)
                    Row(
                      children: widget.actions,
                    )
                ],
              )
            ],
          ),
          ),
      ),
    );
  }
}