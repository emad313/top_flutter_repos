import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';

import '../constants/colors.dart';

class RepoCard extends StatefulWidget {
  final List<dynamic>? tagList;
  final String? title;
  final String? description;
  final String? language;
  final int? stars;
  final String? date;
  final VoidCallback? onTap;
  const RepoCard({
    super.key,
    this.tagList = const [],
    required this.title,
    this.description = '',
    this.language = '',
    this.stars = 0,
    this.date = '',
    this.onTap,
  });

  @override
  State<RepoCard> createState() => _RepoCardState();
}

class _RepoCardState extends State<RepoCard> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onTap,
      child: Container(
                  margin: EdgeInsets.symmetric(vertical: 3.0.h),
                  decoration: BoxDecoration(
                      color: CustomColors.white,
                      borderRadius: BorderRadius.circular(8.0.r),
                      boxShadow: [
                        BoxShadow(
                            color: CustomColors.black.withOpacity(0.1),
                            spreadRadius: -10,
                            blurRadius: 12,
                            offset: const Offset(0, 5))
                      ]),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: 8.0.w, vertical: 8.0.h),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              flex: 1,
                              child: Column(
                                children: [
                                  Icon(
                                    Icons.code,
                                    color: CustomColors.black,
                                    size: 16.0.sp,
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              flex: 9,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    widget.title.toString(),
                                    style: TextStyle(
                                        color: CustomColors.blue,
                                        fontSize: 16.0.sp,
                                        fontWeight: FontWeight.w400),
                                  ),
                                  Text(
                                    widget.description.toString(),
                                    style: TextStyle(
                                        color: CustomColors.black,
                                        fontSize: 11.0.sp,
                                        fontWeight: FontWeight.w400),
                                  ),
                                  Visibility(
                                      visible: widget.tagList!.isNotEmpty,
                                      child: _tagListView()),
                                  SizedBox(height: 8.0.h),
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.star_border_rounded,
                                        color: CustomColors.black,
                                        size: 16.0.sp,
                                      ),
                                      SizedBox(width: 4.0.w),
                                      Text(
                                        NumberFormat.compact().format(widget.stars),
                                        style: TextStyle(
                                            color: CustomColors.black,
                                            fontSize: 12.0.sp,
                                            fontWeight: FontWeight.w400),
                                      ),
                                      SizedBox(width: 8.0.w),
                                      Icon(
                                        Icons.code,
                                        color: CustomColors.black,
                                        size: 16.0.sp,
                                      ),
                                      SizedBox(width: 4.0.w),
                                      Text(
                                        widget.language.toString(),
                                        style: TextStyle(
                                            color: CustomColors.black,
                                            fontSize: 12.0.sp,
                                            fontWeight: FontWeight.w400),
                                      ),
                                      SizedBox(width: 8.0.w),
                                      Icon(
                                        Icons.update,
                                        color: CustomColors.black,
                                        size: 16.0.sp,
                                      ),
                                      SizedBox(width: 4.0.w),
                                      Text(
                                        DateFormat("MMMM d, y").format( DateTime.parse(widget.date.toString())),
                                        style: TextStyle(
                                            color: CustomColors.black,
                                            fontSize: 12.0.sp,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )),
    );
  }
    Visibility _tagListView() {
    return Visibility(
      visible: widget.tagList!.isNotEmpty,
      child: Wrap(
        children: widget.tagList!.map((text) {
          return Padding(
              padding: const EdgeInsets.all(1.0),
              child: FilterChip(
                  backgroundColor: CustomColors.blue.withOpacity(0.8),
                  label: Text(
                    text,
                    style:
                        TextStyle(color: CustomColors.white, fontSize: 10.0.sp),
                  ),
                  onSelected: (value) {}));
        }).toList(),
      ),
    );
  }
}