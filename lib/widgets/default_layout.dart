import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:top_flutter_repo/configs/db_provider.dart';
import 'package:top_flutter_repo/constants/database_constants.dart';

import '../constants/colors.dart';
import '../controllers/settings_controller.dart';
import '../models/repo_data_model.dart';
import '../repositories/api_request.dart';
import 'nav_bar.dart';
import 'navigation_drawer.dart';

class DefaultLayout extends StatefulWidget {
  final List<Widget> widgets;
  const DefaultLayout({
  super.key,
  required this.widgets
  });

  @override
  State<DefaultLayout> createState() => _DefaultLayoutState();
}

class _DefaultLayoutState extends State<DefaultLayout> {
  @override
  void initState() {
    super.initState();
    APIRequest().getGitHubRepos();
  }
  @override
  Widget build(BuildContext context) {
    SettingsController settings = Get.put(SettingsController());
    return Scaffold(
      drawer: const NavigationDrawer(),
      appBar: const NavBar(
        isTitle: true,
        title: 'Top Flutter Repos',
      ),
      body: Obx(() => Padding(
        padding: EdgeInsets.only(left: 8.0.w, right: 8.0.w, top: 8.0.h),
        child: settings.isHomeLoading.value ? Column(
          children: widget.widgets,
        ): const Center(
          child: CircularProgressIndicator(
            color: CustomColors.black,
          ),
        )
      ))
    );
  }
}