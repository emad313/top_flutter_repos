import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_background_service/flutter_background_service.dart';
import 'package:flutter_background_service_android/flutter_background_service_android.dart';
import 'package:flutter_background_service_platform_interface/flutter_background_service_platform_interface.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:top_flutter_repo/constants/colors.dart';
import 'package:top_flutter_repo/controllers/settings_controller.dart';

import '../helpers/scheduler.dart';

class NavigationDrawer extends StatefulWidget {
  const NavigationDrawer({super.key});

  @override
  State<NavigationDrawer> createState() => _NavigationDrawerState();
}

class _NavigationDrawerState extends State<NavigationDrawer> {
  @override
  Widget build(BuildContext context) {
    SettingsController settings = Get.put(SettingsController());
    return Drawer(
      child: SafeArea(
          child: Container(
        color: CustomColors.white,
        child: Padding(
          padding: EdgeInsets.all(8.0.h),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                height: 20.0.h,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20.0.w),
                child: Row(
                  children: [
                    CircleAvatar(
                      radius: 30.0.r,
                      backgroundImage: const CachedNetworkImageProvider(
                          'https://avatars.githubusercontent.com/u/16821659?v=4'),
                    ),
                    SizedBox(
                      width: 20.0.w,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text(
                              'Top Flutter Repo',
                              style: TextStyle(
                                  color: CustomColors.black,
                                  fontSize: 18.0.sp,
                                  fontWeight: FontWeight.w600),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5.0.h,
                        ),
                        Row(
                          children: [
                            Text(
                              'Flutter Developer',
                              style: TextStyle(
                                  color: CustomColors.black,
                                  fontSize: 14.0.sp,
                                  fontWeight: FontWeight.w400),
                            ),
                          ],
                        ),
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 20.0.h,
              ),
              Divider(
                color: CustomColors.black.withOpacity(0.1),
                thickness: 1.0,
              ),

              SizedBox(
                height: 20.0.h,
              ),

              Text(
                'Settings',
                style: TextStyle(
                    color: CustomColors.black,
                    fontSize: 18.0.sp,
                    fontWeight: FontWeight.w600),
              ),

              SizedBox(
                height: 20.0.h,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 5.0.w),
                child: Row(
                  children: [
                    Icon(
                      Icons.timer_outlined,
                      color: CustomColors.black,
                      size: 20.0.sp,
                    ),
                    SizedBox(
                      width: 10.0.w,
                    ),
                    Text(
                      'Every 30 Minutes',
                      style: TextStyle(
                          color: CustomColors.black,
                          fontSize: 16.0.sp,
                          fontWeight: FontWeight.w400),
                    ),
                    const Spacer(
                      flex: 1,
                    ),
                    // switch
                    Obx(() => Switch(
                          value: settings.isScheduler.value,
                          onChanged: (value) async {
                            settings.schedulerToggle();
                            final service = FlutterBackgroundService();
                            var isRunning = await service.isRunning();
                            if (isRunning) {
                              service.invoke("stopService");
                            } else {
                              service.startService();
                            }
                          },
                          activeColor: CustomColors.black,
                          activeTrackColor: CustomColors.black.withOpacity(0.1),
                          inactiveThumbColor: CustomColors.black,
                          inactiveTrackColor:
                              CustomColors.black.withOpacity(0.1),
                        )),
                  ],
                ),
              ),
              // Notification Settings
              SizedBox(
                height: 5.0.h,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 5.0.w),
                child: Row(
                  children: [
                    Icon(
                      Icons.notifications_outlined,
                      color: CustomColors.black,
                      size: 20.0.sp,
                    ),
                    SizedBox(
                      width: 10.0.w,
                    ),
                    Text(
                      'Show Notification',
                      style: TextStyle(
                          color: CustomColors.black,
                          fontSize: 16.0.sp,
                          fontWeight: FontWeight.w400),
                    ),
                    const Spacer(
                      flex: 1,
                    ),
                    // switch
                    Obx(() => Switch(
                          value: settings.isNotification.value,
                          onChanged: (value) async {
                            settings.notificationToggle();
                          },
                          activeColor: CustomColors.black,
                          activeTrackColor: CustomColors.black.withOpacity(0.1),
                          inactiveThumbColor: CustomColors.black,
                          inactiveTrackColor:
                              CustomColors.black.withOpacity(0.1),
                        )),
                  ],
                ),
              ),
            ],
          ),
        ),
      )),
    );
  }
}
