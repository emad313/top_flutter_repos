# top_flutter_repo

## Project Information:


1. Fetch GitHub Repositories with Search Query `Flutter` and `starts` show them on the list
2. Fetched data is stored in local device storage with the `Sqflite Database`. So, the App is to be used in offline mode.
3. Use the `Connectivity_Plus` Plugin to check inter the connection app is online or offline.
4. The repositories list will be scrolling and updated with new 10 items
5. The database will update every 30 min with scheduler functionality. In the project use `Flutter Background Services` plugin for the periodical timer to background process running and data fetch.
6. Here in the project have a setting window for the on/off scheduler.
7. The home page Repositories list will be sorted into two categories - `Update Time` and `Starts Count`
8. Selected Sorting options are selected forever if the app will be closed.
9. All repositories navigated to their details view page.
10. On the details page app, users can see the repository owner's profile picture, name, and project description.
11. Repository details information also store in local storage to access offline.
12. Here in the project use simple functionality with a big vision.
13. This project architect design like it will scale in a large enterprise project.
14. In this project for state management use `GetX`.
15. For the network call use `http` plugin.
16. For the image cache use `Cached Network Image` plugin.
17. For the date time format use `intl` plugin.

## APP Screenshots:
Here is some screenshots of the project

## Home Page:
!![Semantic description of image](/assets/screenshots/home.jpg "Home"){height=800}

## Details Page:
!![Semantic description of image](/assets/screenshots/details.jpg "Details"){height=800}

## Settings Window:
!![Semantic description of image](/assets/screenshots/settings.jpg "Settings"){height=800}

